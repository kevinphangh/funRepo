
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { ReactiveFormsModule } from '@angular/forms';

import { NgxPaginationModule } from 'ngx-pagination';

import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatMenuModule } from '@angular/material/menu';

import { ProductSearchComponent } from './pages/product-search/product-search.component';
import { SearchBarComponent } from './pages/product-search/components/search-bar/search-bar.component';
import { RandomDogComponent } from './pages/random-dog/random-dog.component';
import { HeaderComponent } from './components/header/header.component';
import { CommunicationService } from './services/communication.service';

@NgModule({
  declarations: [
    AppComponent,
    ProductSearchComponent,
    SearchBarComponent,
    HeaderComponent,
    RandomDogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatIconModule,
    MatToolbarModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    MatPaginatorModule,
    MatButtonModule,
    MatMenuModule
  ],
  providers: [CommunicationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
