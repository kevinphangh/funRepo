import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductSearchComponent } from './pages/product-search/product-search.component';
import { RandomDogComponent } from './pages/random-dog/random-dog.component';

const routes: Routes = [
  {path: 'productsearch', component: ProductSearchComponent},
  {path: 'randomdog', component: RandomDogComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
