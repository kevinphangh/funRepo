import { Component } from '@angular/core';
import { debounceTime, exhaustMap, fromEvent, of} from 'rxjs';
import { ElementRef, ViewChild } from '@angular/core';
import { CommunicationService } from '../../services/communication.service';


@Component({
  selector: 'app-random-dog',
  templateUrl: './random-dog.component.html',
  styleUrls: ['./random-dog.component.scss']
})
export class RandomDogComponent {
  dogImageUrl: string = '';

  @ViewChild('dogImage') dogImage!: ElementRef;

  constructor(private communicationService: CommunicationService) {
    this.loadRandomDogImage();
  }

  ngAfterViewInit(): void {
    fromEvent(this.dogImage.nativeElement, 'click')
      .pipe(debounceTime(100))
      .subscribe(() => {
        this.loadRandomDogImage();
      });
  }

  loadRandomDogImage(): void {
    this.communicationService.getRandomDogImage()
      .pipe(
        exhaustMap(response => of(response.message)),
      )
      .subscribe(response => {
        this.dogImageUrl = response;
      })
  }
}
