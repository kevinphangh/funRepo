import { Component, ViewChild } from '@angular/core';
import { CommunicationService } from '../../services/communication.service';
import { Observable, exhaustMap, map, of, catchError } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/assets/products/products';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.scss']
})

export class ProductSearchComponent {

  private firstSearch: boolean = true;
  public productsPerPage: number = 10;
  public products$: Observable<Product[]> = of([]);
  public searchedProducts: Product[] = [];
  public slicedProducts: Product[] = [];

  constructor(private communicationService: CommunicationService, private productService: ProductService) { }

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.communicationService.getSearchValue()
      .pipe(
        map((searchValue) => searchValue.toLowerCase())
      )
      .subscribe(searchValue => {
        if (this.firstSearch) {
          this.productService.getProducts()
            .pipe(
              exhaustMap(products => of(products.content)),
              catchError(error => {
                console.error('Error fetching products: ', error);
                return of([]);
              })
            )
            .subscribe(products => {
              if (!products || products.length <= 0) {
                return; 
              }
              this.firstSearch = false; 
              this.products$ = of(products || []);
              this.findSearchedProducts(products || [], searchValue);
            })
        }
        else {
          this.products$.subscribe(products => {
            this.findSearchedProducts(products, searchValue);
          })
        }
        this.paginator.pageIndex = 0;
      })
  }

  private findSearchedProducts(products: Product[], searchValue: string): void {
    const searchWords = searchValue.split(" ");

    this.searchedProducts = products.filter(product =>
      searchWords.every(word => product.title.toLowerCase().includes(word)
      ))

    if (this.searchedProducts.length < this.productsPerPage) {
      this.slicedProducts = this.searchedProducts.slice(0, this.searchedProducts.length);
    }
    else {
      this.slicedProducts = this.searchedProducts.slice(0, this.productsPerPage);
    }
  }

  onPageChange(event: PageEvent) {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.searchedProducts.length) {
      endIndex = this.searchedProducts.length;
    };
    this.slicedProducts = this.searchedProducts.slice(startIndex, endIndex);
    this.productsPerPage = event.pageSize;
  }
}
