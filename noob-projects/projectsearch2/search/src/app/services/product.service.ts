import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SearchProductResponse } from 'src/assets/products/products';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ProductService {

  private url: string = "/assets/products/products.json";
  
  constructor(private http: HttpClient) { }

  getProducts(): Observable<SearchProductResponse>{
    return this.http.get<SearchProductResponse>(this.url);
  }

}
