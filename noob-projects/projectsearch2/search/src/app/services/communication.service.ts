import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  private _searchValue: Subject<string> = new Subject<string>(); 
  private url = 'https://dog.ceo/api/breeds/image/random';

  constructor(private http:HttpClient) {}

  public sendSearchValue(message: string){
    this._searchValue.next(message);
  }

  public getSearchValue(): Observable<string>{
    return this._searchValue.asObservable();
  }

  getRandomDogImage(): Observable<any>{
    return this.http.get(this.url);
  }

}
