import { Component } from '@angular/core';
import { DogApiService } from 'src/app/services/dog-api.service';

@Component({
  selector: 'app-dog',
  templateUrl: './dog.component.html',
  styleUrls: ['./dog.component.scss']
})
export class DogComponent {

  dogImageUrl: string = '';

  constructor(private dogApiService: DogApiService){
    this.loadRandomDogImage();
  }

  loadRandomDogImage(): void {
    this.dogApiService.getRandomDogImage().subscribe(response => {
      this.dogImageUrl = response.message; 
    })
  }

  
}
