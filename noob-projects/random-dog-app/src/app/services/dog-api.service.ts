import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DogApiService {
  private url = 'https://dog.ceo/api/breeds/image/random';

  constructor(private http:HttpClient) { }

  getRandomDogImage(): Observable<any>{
    return this.http.get(this.url);
  }
}
