import { Component, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject, of } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.css'],
})
export class ProductSearchComponent {
  enteredSearchValue: FormControl = new FormControl('');
  private searchTextChanged: Subject<string> = new Subject<string>();

  @Output()
  searchTextChanged$ = this.searchTextChanged.asObservable();

  constructor() { }

  ngOnInit() {
    this.enteredSearchValue.valueChanges
      .pipe(
        debounceTime(150),
        switchMap((searchValue) => {
          return of(searchValue.toLowerCase());
        })
      )
      .subscribe((searchValue) => {
        this.searchTextChanged.next(searchValue);
      });
  }
}