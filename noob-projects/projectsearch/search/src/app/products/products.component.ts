import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { ProductSearchComponent } from '../product-search/product-search.component';
import { Product } from '../../assets/products.d';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { map, exhaustMap } from 'rxjs/operators';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements AfterViewInit {
  currentPage: number = 1;
  productsPerPage: number = 10;
  products: Observable<Product[]> = of([]);
  searchText: string = '';
  firstSearch: boolean = true;
  searchTriggered = new Subject<string>();

  @ViewChild(ProductSearchComponent) productSearchComponent!: ProductSearchComponent;

  constructor(private http: HttpClient) {}

  ngOnInit(){
    this.searchTriggered.pipe(
      exhaustMap(() => {
        return this.http.get<any>('assets/products.json').pipe(
          map(data => {
            return {
              products: data.content
            };
          })
        );
      })
    ).subscribe(({ products }) => {
      this.products = of(products);
    })
  }

  ngAfterViewInit() {
    this.productSearchComponent.searchTextChanged$.subscribe((searchValue) => {
      if (this.firstSearch) {
        this.searchTriggered.next(searchValue);
        this.firstSearch = false;
      }
      this.currentPage = 1;
      this.searchText = searchValue;
    });
  }

  getFilteredProducts(products: Product[]): Product[] {
    const searchWords = this.searchText.split(" ");
    const filteredProducts = products.filter(product =>
      searchWords.every(word => product.title.toLowerCase().includes(word))
    );
    return filteredProducts;
  }

  getSlicedProducts(products: Product[]): Product[] {
    const filteredProducts = this.getFilteredProducts(products);
    return filteredProducts.slice(
      (this.currentPage - 1) * this.productsPerPage,
      this.currentPage * this.productsPerPage
    );
  }

  getTotalPages(products: Product[]): number {
    if (!products) return 0;

    const filteredProducts = this.getFilteredProducts(products);
    let totalPages: number = Math.ceil(filteredProducts.length / this.productsPerPage);

    if (totalPages > 0) return totalPages;
    return 1;
  }

  goToPage(page: number) {
    this.currentPage = page;
  }
}