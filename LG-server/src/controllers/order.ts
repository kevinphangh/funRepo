import { Request, Response, NextFunction } from "express";
import logging from "../config/logging";
import { Connect, Query } from "../config/mysql";

const NAMESPACE = "Orders";

const getOrders = (req: Request, res: Response, next: NextFunction) => {
  logging.info(NAMESPACE, "Getting orders");
};
