import { Request, Response, NextFunction } from "express";
import logging from "../config/logging";
import { Connect, Query } from "../config/mysql";

const NAMESPACE = "Racks";

const insertRack = (req: Request, res: Response, next: NextFunction) => {
  logging.info(NAMESPACE, "Inserting rack.");

  let { rackNo, side, shelfCount, startPosX, endPosX } = req.body;

  let query = `INSERT INTO racks (rackNo, side, shelfCount, startPosX, endPosX) VALUES
     ("${rackNo}", "${side}", "${shelfCount}", "${startPosX}", "${endPosX}")`;

  Connect()
    .then((connection) => {
      Query(connection, query)
        .then((result) => {
          return res.status(200).json({
            result,
          });
        })
        .catch((error) => {
          logging.error(NAMESPACE, error.message, error);
          return res.status(500).json({
            message: error.message,
            error,
          });
        });
    })
    .catch((error) => {
      logging.error(NAMESPACE, error.message, error);
      return res.status(500).json({
        message: error.message,
        error,
      });
    });
};

const deleteRack = (req: Request, res: Response, next: NextFunction) => {
  logging.info(NAMESPACE, "Deleting rack");

  let rackNo = req.params.rackNo;

  let query = `DELETE FROM racks WHERE rackNo = ${rackNo}`;

  Connect()
    .then((connection) => {
      Query(connection, query)
        .then((result) => {
          return res.status(200).json({
            result,
          });
        })
        .catch((error) => {
          logging.error(NAMESPACE, error.message, error);
          return res.status(500).json({
            message: error.message,
            error,
          });
        });
    })
    .catch((error) => {
      logging.error(NAMESPACE, error.message, error);
      return res.status(500).json({
        message: error.message,
        error,
      });
    });
};

const getRacks = (req: Request, res: Response, next: NextFunction) => {
  logging.info(NAMESPACE, "Getting all racks");

  let query = "SELECT * FROM racks";

  Connect()
    .then((connection) => {
      Query(connection, query)
        .then((result) => {
          return res.status(200).json({
            result,
          });
        })
        .catch((error) => {
          logging.error(NAMESPACE, error.message, error);
          return res.status(500).json({
            message: error.message,
            error,
          });
        });
    })
    .catch((error) => {
      logging.error(NAMESPACE, error.message, error);
      return res.status(500).json({
        message: error.message,
        error,
      });
    });
};

export default { insertRack, deleteRack, getRacks };
