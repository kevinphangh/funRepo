import express from "express";
import controller from "../controllers/rack";

const router = express.Router();

router.post("/insert", controller.insertRack);
router.delete("/delete/:rackNo", controller.deleteRack);
router.get("/", controller.getRacks);

export default router;
