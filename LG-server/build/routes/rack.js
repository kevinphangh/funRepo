"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const rack_1 = __importDefault(require("../controllers/rack"));
const router = express_1.default.Router();
router.post("/insert", rack_1.default.insertRack);
router.delete("/delete/:rackNo", rack_1.default.deleteRack);
router.get("/", rack_1.default.getRacks);
exports.default = router;
