"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logging_1 = __importDefault(require("../config/logging"));
const mysql_1 = require("../config/mysql");
const NAMESPACE = "Racks";
const insertRack = (req, res, next) => {
    logging_1.default.info(NAMESPACE, "Inserting rack.");
    let { rackNo, side, shelfCount, startPosX, endPosX } = req.body;
    let query = `INSERT INTO racks (rackNo, side, shelfCount, startPosX, endPosX) VALUES
     ("${rackNo}", "${side}", "${shelfCount}", "${startPosX}", "${endPosX}")`;
    (0, mysql_1.Connect)()
        .then((connection) => {
        (0, mysql_1.Query)(connection, query)
            .then((result) => {
            return res.status(200).json({
                result,
            });
        })
            .catch((error) => {
            logging_1.default.error(NAMESPACE, error.message, error);
            return res.status(500).json({
                message: error.message,
                error,
            });
        });
    })
        .catch((error) => {
        logging_1.default.error(NAMESPACE, error.message, error);
        return res.status(500).json({
            message: error.message,
            error,
        });
    });
};
const deleteRack = (req, res, next) => {
    logging_1.default.info(NAMESPACE, "Deleting rack");
    let rackNo = req.params.rackNo;
    let query = `DELETE FROM racks WHERE rackNo = ${rackNo}`;
    (0, mysql_1.Connect)()
        .then((connection) => {
        (0, mysql_1.Query)(connection, query)
            .then((result) => {
            return res.status(200).json({
                result,
            });
        })
            .catch((error) => {
            logging_1.default.error(NAMESPACE, error.message, error);
            return res.status(500).json({
                message: error.message,
                error,
            });
        });
    })
        .catch((error) => {
        logging_1.default.error(NAMESPACE, error.message, error);
        return res.status(500).json({
            message: error.message,
            error,
        });
    });
};
const getRacks = (req, res, next) => {
    logging_1.default.info(NAMESPACE, "Getting all racks");
    let query = "SELECT * FROM racks";
    (0, mysql_1.Connect)()
        .then((connection) => {
        (0, mysql_1.Query)(connection, query)
            .then((result) => {
            return res.status(200).json({
                result,
            });
        })
            .catch((error) => {
            logging_1.default.error(NAMESPACE, error.message, error);
            return res.status(500).json({
                message: error.message,
                error,
            });
        });
    })
        .catch((error) => {
        logging_1.default.error(NAMESPACE, error.message, error);
        return res.status(500).json({
            message: error.message,
            error,
        });
    });
};
exports.default = { insertRack, deleteRack, getRacks };
