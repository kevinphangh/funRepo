import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DogImage } from 'src/assets/random-dog/random-dog.interface';

@Injectable({
  providedIn: 'root'
})
export class RandomDogService {

  constructor(private http: HttpClient) { }

  private url = 'https://dog.ceo/api/breeds/image/random';

  getRandomDogImage(): Observable<DogImage>{
    return this.http.get<DogImage>(this.url);
  }
}
