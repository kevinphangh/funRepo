import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {
  private _searchValue: Subject<string> = new Subject<string>(); 

  constructor() {}

  public sendSearchValue(message: string){
    this._searchValue.next(message);
  }

  public getSearchValue(): Observable<string>{
    return this._searchValue.asObservable();
  }


}
