import { Component } from '@angular/core';
import { catchError, debounceTime, exhaustMap, fromEvent, of } from 'rxjs';
import { ElementRef, ViewChild } from '@angular/core';
import { RandomDogService } from 'src/app/services/random-dog.service';

@Component({
  selector: 'app-random-dog',
  templateUrl: './random-dog.component.html',
  styleUrls: ['./random-dog.component.scss']
})
export class RandomDogComponent {
  dogImageUrl: string = '';
  imageLoadRetries: number = 0;
  firstPic: boolean = true;

  @ViewChild('dogImage') dogImage!: ElementRef;

  constructor(private randomDogService: RandomDogService) { }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    fromEvent(this.dogImage.nativeElement, 'click')
      .pipe(
        debounceTime(100)
      )
      .subscribe(() => {
        this.loadRandomDogImage();
      })
  }

  loadRandomDogImage(): void {
    this.randomDogService.getRandomDogImage()
      .pipe(
        exhaustMap(response => of(response.message)),
        catchError(error => {
          console.log('Error getting dog picture', error);
          return of(null);
        })
      )
      .subscribe(response => {
        if (!response) return;
        this.dogImageUrl = response;
      })
  }

  onLoadImage(): void {
    this.imageLoadRetries = 0;
    console.log('Sucessfully loaded image');
  }

  onImageError(): void {
    if (this.firstPic) {
      this.firstPic = false;
      this.loadRandomDogImage();
    }
    else if (this.imageLoadRetries <= 3) {
      console.log('Sucessfully got dog URL from API, but image is broken, retrying...')
      this.loadRandomDogImage();
    }
    else {
      console.log('Failed loading new dog image 3 times');
      window.alert('Something might be wrong with those dogs \n cant show images ')
      this.imageLoadRetries = 0;
    }
  }
}
