import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RandomDogComponent } from './random-dog.component';
import { RandomDogService } from 'src/app/services/random-dog.service';
import { of, throwError } from 'rxjs';
import { DogImage } from 'src/assets/random-dog/random-dog.interface';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ElementRef } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('RandomDogComponent', () => {
  let component: RandomDogComponent;
  let fixture: ComponentFixture<RandomDogComponent>;
  let randomDogServiceMock: jasmine.SpyObj<RandomDogService>;

  class ElementRefMock {
    nativeElement = {
      click: () => { }
    }
  }

  beforeEach(async () => {
    const spy = jasmine.createSpyObj('RandomDogService', ['getRandomDogImage']);
    let service: RandomDogService;

    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      declarations: [RandomDogComponent],
      providers: [
        { provide: RandomDogService, useValue: spy },
        { provide: ElementRef, useClass: ElementRefMock }]

    })
      .compileComponents();

    service = TestBed.inject(RandomDogService);
    randomDogServiceMock = TestBed.inject(RandomDogService) as jasmine.SpyObj<RandomDogService>;
    fixture = TestBed.createComponent(RandomDogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an empty initial dogImageUrl', () => {
    expect(component.dogImageUrl).toBe('');
  })

  it('should have an initial imageLoadRetries value 0', () => {
    expect(component.imageLoadRetries).toBe(0);
  })

  it('should load a random dog image on sucessful API call', () => {
    const imageUrl = 'https://images.dog.ceo/breeds/terrier-norwich/n02094258_100.jpg';
    randomDogServiceMock.getRandomDogImage.and.returnValue(of({ message: imageUrl } as DogImage));

    component.loadRandomDogImage();
    expect(component.dogImageUrl).toBe(imageUrl);
  })

  it('should retry loading a random dog image on failed API call', () => {
    const imageUrl = 'https://images.dog.ceo/breeds/terrier-norwich/n02094258_100.jpg';
    randomDogServiceMock.getRandomDogImage.and.returnValue(throwError('Error'));

    component.loadRandomDogImage();
    expect(component.dogImageUrl).toBe('');
  })

  it('should reset imageLoadRetries to 0 on sucessful image load', () => {
    component.imageLoadRetries = 2;
    component.onLoadImage();
    expect(component.imageLoadRetries).toBe(0);
  })

  /*
  it('should call loadRandomDogImage when the dog image is clicked', () => {
    const loadRandomDogImageSpy = spyOn(component, 'loadRandomDogImage');
    const dogImageElement = fixture.debugElement.query(By.css('#dogImage'));

    dogImageElement.triggerEventHandler('click', null);

    expect(loadRandomDogImageSpy).toHaveBeenCalled();
  })
  */
 
});
