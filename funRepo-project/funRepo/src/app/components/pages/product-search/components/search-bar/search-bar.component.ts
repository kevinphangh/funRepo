import { Component} from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs';
import { CommunicationService } from 'src/app/services/communication.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})

export class SearchBarComponent {
  public searchControl: FormControl = new FormControl('');

  constructor(private communicationService: CommunicationService) {
    this.searchControl.valueChanges.pipe(debounceTime(150))
      .subscribe(searchValue => {
        this.communicationService.sendSearchValue(searchValue);
      })
  }
}


