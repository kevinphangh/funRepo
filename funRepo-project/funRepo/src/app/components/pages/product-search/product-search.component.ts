import { Component, ViewChild } from '@angular/core';
import { CommunicationService } from 'src/app/services/communication.service';
import { Observable, exhaustMap, of, catchError, switchMap } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/assets/products/products';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.scss']
})

export class ProductSearchComponent {

  private firstSearch: boolean = true;
  productsPerPage: number = 10;
  products$: Observable<Product[]> = of([]);
  searchedProducts: Product[] = [];
  slicedProducts: Product[] = [];
  searchValue: string = '';

  constructor(private communicationService: CommunicationService, private productService: ProductService) { }

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.communicationService.getSearchValue()
      .pipe(
        switchMap(searchValue => of(searchValue.toLowerCase())),
        catchError(error => {
          console.log('Error in communication service getting search value: ' + error);
          return of(null);
        }),
      )
      .subscribe(searchValue => {
        if (searchValue === null) {
          return;
        }
        if (this.firstSearch) {
          this.firstSearch = false;
          this.productService.getProducts()
            .pipe(
              exhaustMap(products => of(products.content)),
              catchError(error => {
                console.log('Error getting products: ', error);
                return of(null);
              })
            )
            .subscribe(products => {
              if (!products) {
                this.firstSearch = true;
                return;
              }
              this.products$ = of(products || []);
              this.findSearchedProducts(products || [], searchValue);
            }
            );
        }
        else {
          this.products$.subscribe(products => {
            this.findSearchedProducts(products, searchValue);
          });
        }
      }
      );
  }

  private findSearchedProducts(products: Product[], searchValue: string): void {
    if (!searchValue) {
      this.searchedProducts = products;
      this.sliceProducts(products);
      return;
    }

    const searchWords = searchValue.split(" ");
    this.searchedProducts = products.filter(product =>
      searchWords.every(word => product.title.toLowerCase().includes(word)
      ))
    this.sliceProducts(this.searchedProducts);
  }

  private sliceProducts(products: Product[]): void {
    if (products.length < this.productsPerPage) {
      this.slicedProducts = products.slice(0, products.length);
    }
    else {
      this.slicedProducts = products.slice(0, this.productsPerPage);
    }

    if (this.paginator) {
      this.resetPaginator();
    }
  }

  resetPaginator(): void {
    this.paginator.pageIndex = 0;
  }

  onPageChange(event: PageEvent) {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.searchedProducts.length) {
      endIndex = this.searchedProducts.length;
    };
    this.slicedProducts = this.searchedProducts.slice(startIndex, endIndex);
    this.productsPerPage = event.pageSize;
  }
}
