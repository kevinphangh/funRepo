import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductSearchComponent } from './components/pages/product-search/product-search.component';
import { RandomDogComponent } from './components/pages/random-dog/random-dog.component';
import { HomepageComponent } from './components/pages/homepage/homepage.component';

const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'productsearch', component: ProductSearchComponent},
  {path: 'randomdog', component: RandomDogComponent},
  {path: 'homepage', component: HomepageComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
